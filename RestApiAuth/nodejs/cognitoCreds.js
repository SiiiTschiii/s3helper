const AWS = require('aws-sdk');

// set the default config object
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'eu-central-1:497ae7fd-7d32-40b0-b8bd-df88ad13ec4f'
});

AWS.config.update({region: 'eu-central-1'});

console.log(AWS.config.credentials.accessKeyId);
console.log(AWS.config.credentials.sessionToken);

