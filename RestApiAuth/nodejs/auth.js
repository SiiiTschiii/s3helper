var crypto = require("crypto-js");
var https = require('https');

var access_key = process.env.AWS_ACCESS_KEY_ID;
var secret_key = process.env.AWS_SECRET_ACCESS_KEY;
var region = 'eu-central-1';
var s3Bucket = 'composey-media';
var s3Key = 'hls/TheBigBangTheory/The.Big.Bang.Theory.S01E01.720p.HDTV.X264-MRSK/master.m3u8';
var host = 's3.'+region+'.amazonaws.com';
var myService = 's3';
var myMethod = 'GET';
var myPath = '/'+s3Bucket+'/'+s3Key;

function getSignatureKey(keyDirty, dateStampDirty, regionNameDirty, serviceNameDirty) {

    var key = crypto.enc.Utf8.parse('AWS4' + keyDirty);
    var dateStamp = crypto.enc.Utf8.parse(dateStampDirty);
    var regionName = crypto.enc.Utf8.parse(regionNameDirty);
    var serviceName = crypto.enc.Utf8.parse(serviceNameDirty);

    var kDate = crypto.HmacSHA256(dateStamp, key);
    var kRegion = crypto.HmacSHA256(regionName, kDate);
    var kService = crypto.HmacSHA256(serviceName, kRegion);
    var kSigning = crypto.HmacSHA256("aws4_request", kService);
    return kSigning;
}

// this function converts the generic JS ISO8601 date format to the specific format the AWS API wants
function getAmzDate(dateStr) {
    var chars = [":","-"];
    for (var i=0;i<chars.length;i++) {
        while (dateStr.indexOf(chars[i]) != -1) {
            dateStr = dateStr.replace(chars[i],"");
        }
    }
    dateStr = dateStr.split(".")[0] + "Z";
    return dateStr;
}


// get the various date formats needed to form our request
var amzDate = getAmzDate(new Date().toISOString());
var datestamp = amzDate.split("T")[0];

console.log('amzdate:'+amzDate);
console.log('datestamp:'+datestamp);

// we have an empty payload here because it is a GET request
var payload = '';
// get the SHA256 hash value for our payload
var hashedPayload = crypto.SHA256(payload).toString();
console.log('payload_hash:'+hashedPayload);

// create our canonical request
var canonicalReq =  myMethod + '\n' +
    myPath + '\n' +
    '\n' +
    'host:' + host + '\n' +
    'x-amz-date:' + amzDate + '\n' +
    '\n' +
    'host;x-amz-date' + '\n' +
    hashedPayload;

console.log('canonicalReq:'+canonicalReq);

// hash the canonical request
var canonicalReqHash = crypto.SHA256(canonicalReq).toString();
console.log('canonicalReqHash:'+ canonicalReqHash)

// form our String-to-Sign
var stringToSign =  'AWS4-HMAC-SHA256\n' +
    amzDate + '\n' +
    datestamp+'/'+region+'/'+myService+'/aws4_request\n'+
    canonicalReqHash;
console.log('stringToSign: '+ stringToSign);
console.log('string_to_siogn_hash: '+crypto.SHA256(stringToSign).toString())

// get our Signing Key
var signingKey = getSignatureKey(secret_key, datestamp, region, myService);
//signingKey = '16f2b12273787ea0ea8981f73df916a1225d1153003e1705c6fa69c57c8aa8ef';
console.log('signingKey: '+signingKey);

// Sign our String-to-Sign with our Signing Key
var authKey = crypto.HmacSHA256(stringToSign, signingKey);
console.log('authKey: '+authKey);

// Form our authorization header
var authString  = 'AWS4-HMAC-SHA256 ' +
    'Credential='+
    access_key+'/'+
    datestamp+'/'+
    region+'/'+
    myService+'/aws4_request, '+
    'SignedHeaders=host;x-amz-date, '+
    'Signature='+authKey;

console.log('authString: '+authString)

// throw our headers together
headers = {
    'Authorization' : authString,
    'Host' : host,
    'x-amz-date' : amzDate,
    'x-amz-content-sha256' : hashedPayload
};

// the REST API call using the Node.js 'https' module
function performRequest(endpoint, path, headers, data, success) {

    var dataString = data;

    var options = {
        host: endpoint,
        port: 443,
        path: path,
        method: 'GET',
        headers: headers
    };
    console.log('options: %O', options);

    var req = https.request(options, function(res) {
        res.setEncoding('utf-8');

        var responseString = '';

        res.on('data', function(data) {
            responseString += data;
        });

        res.on('end', function() {
            //console.log(responseString);
            success(responseString);
        });
    });

    req.write(dataString);
    req.end();
}

console.log('\nBEGIN REQUEST++++++++++++++++++++++++++++++++++++');
var path = '/'+ s3Bucket + '/' + s3Key

// call our function
performRequest(host, path, headers, payload, function(response) {
    // parse the response from our function and write the results to the console
    console.log('\nBEGIN RESPONSE++++++++++++++++++++++++++++++++++++');
    console.log(response);
});
