# s3helper

[![pipeline status](https://gitlab.ethz.ch/chgerber/s3helper/badges/master/pipeline.svg)](https://gitlab.ethz.ch/chgerber/s3helper/commits/master)
[![coverage report](https://gitlab.ethz.ch/chgerber/s3helper/badges/master/coverage.svg)](https://gitlab.ethz.ch/chgerber/s3helper/commits/master)

Go helper methods for interaction with aws s3

### Requirements
Env. Variables need to be set which correspong to an IAM role with full S3 acceess
```bash
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
```
