package s3helper

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	log "github.com/sirupsen/logrus"
	"gitlab.ethz.ch/chgerber/monitor"
	"os"
	"path"
	"path/filepath"
	"strings"
)

// UploadAll uploads all files (recursively) in the given dir which have one of the given suffix to the given s3 bucket. s3 key = filename
func UploadAll(s *session.Session, bucket string, prefix string, srcDir string, suffix []string) (num int, err error) {

	err = filepath.Walk(srcDir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if !info.IsDir() {
				for _, suffix := range suffix {
					if filepath.Ext(path) == suffix {
						s3Key := filepath.Join(prefix, filepath.Base(path))
						log.Tracef("%v %s", num, s3Key)

						s3UpInput := s3manager.UploadInput{
							Bucket: aws.String(bucket),
							Key:    aws.String(s3Key),
						}

						_, err := UploadS3Obj(s, s3UpInput, path)
						if err != nil {
							return err
						}
						num++
					}
				}

			}
			return nil
		})
	if err != nil {
		return num, err
	}
	return num, nil

}

// UploadS3Obj uploads the file to the given s3 bucket/key with an expiry time of 24h. Overwrites s3 object if already exists.
func UploadS3Obj(s *session.Session, s3UpInput s3manager.UploadInput, srcFile string) (location string, err error) {
	defer monitor.Elapsed()()

	// Create an uploader with the session and default options
	uploader := s3manager.NewUploader(s)

	f, err := os.Open(srcFile)
	if err != nil {
		return "", err
	}

	s3UpInput.Body = f

	// Upload the file to S3.
	result, err := uploader.Upload(&s3UpInput)
	if err != nil {
		return "", err
	}
	return result.Location, nil
}

// DownloadS3Obj saves the s3 object at bucket/key as file in the given folder named as filepath.Base(key)
func DownloadS3Obj(s *session.Session, bucket string, key string, dstFolder string) (file string, err error) {
	defer monitor.Elapsed()()

	// Create a file to write the S3 Object contents to.
	f, err := os.Create(filepath.Join(dstFolder, path.Base(key)))
	if err != nil {
		return "", err
	}

	d := s3manager.NewDownloader(s)

	log.Tracef("Download S3 object %s%s", bucket, key)
	// Write the contents of S3 Object to the file
	_, err = d.Download(f, &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})
	if err != nil {
		return "", err
	}
	return f.Name(), nil
}

// SubMediaObj describes the mapping of Subtitle and Media object (e.g video file and the corresponding subtitle file)
type SubMediaObj struct {
	Sub   *s3.Object
	Media *s3.Object
}

// VideoExists finds all subtitle obj in subs that have a matching video obj in objects with one of the supported suffix (video type indicator) and returns them as pairs.
func VideoExists(objects []*s3.Object, subs []*s3.Object, videoSuffix []string) []*SubMediaObj {

	objMap := make(map[string]*s3.Object)
	for _, object := range objects {
		objMap[*object.Key] = object
	}

	var smo []*SubMediaObj

	for _, subObj := range subs {
		keyWithoutSuffix := strings.TrimSuffix(*subObj.Key, filepath.Ext(*subObj.Key))
		for _, suffix := range videoSuffix {
			videoObjKey := keyWithoutSuffix + suffix
			if videoObj, ok := objMap[videoObjKey]; ok {
				log.Tracef("%s has subtitle", videoObjKey)
				//do something here
				smo = append(smo, &SubMediaObj{subObj, videoObj})
				break
			}
		}
	}

	return smo
}

// FindSubs returns all objects that have a filename with the given suffix
// subSuffix need to have a heading dot (e.g.: .srt)
func FindSubs(objects []*s3.Object, subSuffix []string) (subs []*s3.Object) {

	for _, object := range objects {
		for _, suffix := range subSuffix {
			if filepath.Ext(*object.Key) == suffix {
				log.Tracef("%s found", *object.Key)
				subs = append(subs, object)
				break
			}
		}
	}

	return subs
}

// ListS3Obj returns all objects in the passed bucket with the passed prefix
func ListS3Obj(s *session.Session, bucket string, prefix string) ([]*s3.Object, error) {

	//if prefix != "" {
	//	prefix += "/"
	//}

	client := s3.New(s)

	var res *s3.ListObjectsOutput
	var err error
	if prefix == "" {
		res, err = client.ListObjects(&s3.ListObjectsInput{
			Bucket: aws.String(bucket),
			//Delimiter: aws.String("/"),
		})
		if err != nil {
			return nil, err
		}
	} else {
		res, err = client.ListObjects(&s3.ListObjectsInput{
			Bucket: aws.String(bucket),
			Prefix: aws.String(prefix),
			//Delimiter: aws.String("/"),
		})
		if err != nil {
			return nil, err
		}
	}

	log.Tracef("%v matching objects in s3://%s/%s", len(res.Contents), *res.Name, *res.Prefix)
	return res.Contents, nil
}
