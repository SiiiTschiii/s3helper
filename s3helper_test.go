package s3helper

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"
)

var subSuffix = []string{".srt"}

func TestListAllS3Objects(t *testing.T) {

	// The s the S3 Downloader will use
	s := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String("eu-central-1")},
	}))

	s3BucketMedia := "arnie-project-tests"
	s3PrefixSub := "hi-thanks-bye-60seconds"

	objList, err := ListS3Obj(s, s3BucketMedia, s3PrefixSub)
	assert.Equal(t, nil, err)
	assert.Equal(t, 7, len(objList))
	assert.Equal(t, s3PrefixSub+"/", *objList[0].Key)
	assert.Equal(t, s3PrefixSub+"/bye.mkv", *objList[1].Key)
}

func TestFindS3Subs(t *testing.T) {

	// The s the S3 Downloader will use
	s := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String("eu-central-1")},
	}))

	s3BucketMedia := "arnie-project-tests"
	s3PrefixSub := "hi-thanks-bye-60seconds"

	objList, err := ListS3Obj(s, s3BucketMedia, s3PrefixSub)
	assert.Equal(t, nil, err)

	subList := FindSubs(objList, subSuffix)
	assert.Equal(t, nil, err)

	assert.Equal(t, 3, len(subList))
	assert.Equal(t, s3PrefixSub+"/bye.srt", *subList[0].Key)
}

func TestFindSubsWithMediaS3(t *testing.T) {

	// The s the S3 Downloader will use
	s := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String("eu-central-1")},
	}))

	s3BucketMedia := "arnie-project-media"
	s3PrefixSub := "TheBigBangTheory"

	objList, err := ListS3Obj(s, s3BucketMedia, s3PrefixSub)
	assert.Equal(t, nil, err)

	subList := FindSubs(objList, subSuffix)
	assert.Equal(t, nil, err)

	smo := VideoExists(objList, subList, []string{".mp4"})
	assert.Equal(t, 0, len(smo))

	smo = VideoExists(objList, subList, []string{".mp4", ".mkv"})
	assert.Equal(t, 253, len(smo))
	assert.Equal(t, s3PrefixSub+"/The.Big.Bang.Theory.S01E01.720p.HDTV.X264-MRSK.srt", *smo[0].Sub.Key)
	assert.Equal(t, s3PrefixSub+"/The.Big.Bang.Theory.S01E01.720p.HDTV.X264-MRSK.mkv", *smo[0].Media.Key)

}

func TestFindDownloadOneSubS3(t *testing.T) {

	// The s the S3 Downloader will use
	s := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String("eu-central-1")},
	}))

	s3BucketMedia := "arnie-project-media"
	s3PrefixSub := "TheBigBangTheory"

	objList, err := ListS3Obj(s, s3BucketMedia, s3PrefixSub)
	assert.Equal(t, nil, err)

	subList := FindSubs(objList, subSuffix)
	assert.Equal(t, nil, err)

	smo := VideoExists(objList, subList, []string{".mp4", ".mkv"})
	assert.Equal(t, true, len(smo) > 0)

	dstDir := "/tmp"
	file, err := DownloadS3Obj(s, s3BucketMedia, *smo[0].Sub.Key, dstDir)
	assert.Equal(t, nil, err)

	_, err = os.Stat(file)
	assert.Equal(t, false, os.IsNotExist(err))
	assert.Equal(t, nil, err)

	//clean env.
	root := strings.Split(*smo[0].Sub.Key, "/")[0]
	assert.Equal(t, filepath.Join(dstDir, s3PrefixSub), filepath.Join(dstDir, root))

	err = os.RemoveAll(filepath.Join(dstDir, root))
	assert.Equal(t, nil, err)
}

func TestUploadFileToS3(t *testing.T) {
	s3BucketCandidates := "arnie-project-tests"
	filename := "/tmp/dat1"

	d1 := []byte("hello\ngo\n")
	err := ioutil.WriteFile(filename, d1, 0644)
	assert.Equal(t, nil, err)

	s := session.Must(session.NewSessionWithOptions(session.Options{
		Config: aws.Config{Region: aws.String("eu-central-1")},
	}))

	s3UpInput := s3manager.UploadInput{
		Bucket: aws.String(s3BucketCandidates),
		Key:    aws.String(filepath.Base(filename)),
	}
	location, err := UploadS3Obj(s, s3UpInput, filename)
	assert.Equal(t, nil, err)
	assert.Equal(t, "https://"+s3BucketCandidates+".s3.eu-central-1.amazonaws.com/"+*s3UpInput.Key, location)

	err = os.Remove(filename)
	assert.Equal(t, nil, err)
}

// This is not a test it uploads the video and sub files of a local dir to s3
//func TestUploadAll(t *testing.T) {
//
//	//The s the S3 Downloader will use
//	s := session.Must(session.NewSessionWithOptions(session.Options{
//		Config: aws.Config{Region: aws.String("eu-central-1")},
//	}))
//
//	s3BucketMedia := "arnie-project-media"
//
//	num, err := UploadAll(s, s3BucketMedia, "TheBigBangTheory","/media/christof/Data/movies/TheBigBangTheory", []string{".mkv", ".mp4", ".srt"})
//	assert.Equal(t, nil, err)
//	assert.Equal(t, 525, num)
//}
