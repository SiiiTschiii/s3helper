module gitlab.ethz.ch/chgerber/s3helper

go 1.12

require (
	github.com/aws/aws-sdk-go v1.19.5
	github.com/sirupsen/logrus v1.4.1
	github.com/stretchr/testify v1.3.0
	gitlab.ethz.ch/chgerber/monitor v0.0.0-20190527191251-2bb9dd731340
)
